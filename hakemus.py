import re
import datetime

class Hakemus:

    __months = ["", "tammikuu", "helmikuu", "maaliskuu", "huhtikuu",
              "toukokuu", "kesäkuu", "heinäkuu", "elokuu", "syyskuu",
              "lokakuu", "marraskuu", "joulukuu"]

    __months_re = "|".join(__months[1:])

    __pv_re = " ".join(["([1-3]?[0-9]).",
                      "(" + __months_re + ")ta",
                      "(20[0-9][0-9])",
                      "([0-2]?[0-9]):([0-5][0-9])"
                      ])


    def __init__(self, jättöpäivä, tila):
        self.tila = tila

        search = re.search(self.__pv_re, jättöpäivä)
        if search:
            self.jättöpäivä = datetime.datetime(
                day=int(search.group(1)),
                month=self.__months.index(search.group(2)),
                year=int(search.group(3)),
                hour=int(search.group(4)),
                minute=int(search.group(5))
                )
        else:
            self.jättöpäivä = None
