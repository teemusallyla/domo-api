from domo import Domo
import sqlite3
import time
import sys
import datetime
import os

firefox = "firefox" in sys.argv
loop = "loop" in sys.argv

conn = sqlite3.connect("domo.db")
cur = conn.cursor()

d = Domo(firefox)

if not "email.txt" in os.listdir():
    email = input("What's your email? ")
    with open("email.txt", "w+") as f:
        f.write(email)
else:
    with open("email.txt") as f:
        email = f.read()

d.login(email)

last_t = cur.execute("select max(time) from jonot;").fetchone()[0]
last_fetch = datetime.datetime.fromtimestamp(last_t)

try:
    while True:
        now = datetime.datetime.now()
        diff = now - last_fetch
        if diff < datetime.timedelta(days=1):
            wait_time = (last_fetch + datetime.timedelta(days=1)) - now
            print("Sleeping for {:d} seconds".format(wait_time.seconds))
            for x in range(wait_time.seconds):
                time.sleep(1)
            d.refresh()
        last_fetch = datetime.datetime.now()

        jono_map = {}
        for jono in d.jonot:
            jono_map[jono["nimi"].split()[0].lower()] = jono["paras sija"]
        command = "INSERT INTO jonot ({}) VALUES ({})".format(
                ", ".join(["time"] + list(jono_map.keys())),
                ("?, " * (len(jono_map) + 1))[:-2])
        cur.execute(
                command,
                [int(time.time())] + list(jono_map.values())
        )
        conn.commit()
        print(str(datetime.datetime.now()) + ": fetched queues")

        if not loop:
            break

        for x in range(24*60*60):
            time.sleep(1)
        d.refresh()
except Exception as e:
    print(e)
except KeyboardInterrupt:
    print("Stopping")
finally:
    conn.close()
    d.close()
