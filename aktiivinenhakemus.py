import hakemus

class AktiivinenHakemus(hakemus.Hakemus):

    def __init__(self, jättöpäivä, tila, numero, jonot):
        super().__init__(jättöpäivä, tila)
        self.numero = numero
        self.jonot = jonot

    def luo_hakemus(box):
        jono_str = box.text.split("\n")
        numero = int(jono_str[0].split("#")[1])
        pv = jono_str[2]
        tila = jono_str[4]
        jonot = box.find_elements_by_tag_name("li")
        asuntojonot = []
        for jono in jonot:
            jono_jono = jono.text.split("\n")
            nimi = jono_jono[0]
            lkm = jono_jono[1]
            paras = jono_jono[2]
            obj = {"nimi": nimi,
                    "paras sija": paras,
                    "asuntoja": lkm}
            asuntojonot.append(obj)

        return AktiivinenHakemus(
                pv,
                tila,
                numero,
                asuntojonot
                )
