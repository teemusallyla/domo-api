from selenium import webdriver
import getpass
import re
from imports import *
import datetime
import sys
import time

class Domo:

    __date_re = "([1-3]?[0-9]).(1?[0-9]).(20[0-9][0-9])"

    def __init__(self, firefox=False):
        self.hakemukset = None
        self.sopimukset = None
        self.asumisoikeus = None
        self.arkisto = None
        self.browser = None
        self.jonot = None
        self.firefox = firefox


    def login(self, email):
        b = None
        try:
            if self.firefox:
                op = webdriver.firefox.options.Options()
                op.headless = False
                b = webdriver.Firefox(options=op)
            else:
                op = webdriver.chrome.options.Options()
                op.headless = True
                b = webdriver.Chrome(options=op)
            b.implicitly_wait(5)
            b.get("https://domo.ayy.fi/customers/sign_in")
            
            email_input = b.find_element_by_css_selector("input.email")
            email_input.send_keys(email)

            passw_input = b.find_element_by_css_selector("input.password")
            passw_input.send_keys(getpass.getpass("Enter your password: "))
            passw_input.submit()
            time.sleep(0.5)
            self.browser = b
            self.refresh()
        except Exception as e:
            print(e)
            if b:
                b.quit()
        
        return b

    def refresh(self):
        if not self.browser:
            print("Not logged in or disconnected. Run login first")
        else:
            b = self.browser
            b.get("https://domo.ayy.fi")
            ao = b.find_element_by_css_selector("div.row > div.grid-12 > p")
            self.__set_asumisoikeus(ao.text)
            leases = b.find_elements_by_class_name("list-item")
            self.sopimukset = []
            for lease in leases:
                l_asunto = lease.text.split("\n")[0]
                l_tila = lease.text.split("\n")[1]
                sopimus = {
                        "nimi": l_asunto,
                        "tila": l_tila
                        }

                self.sopimukset.append(sopimus)
            boxes = b.find_elements_by_class_name("application-box")
            self.hakemukset = []
            for h in boxes:
                self.hakemukset.append(AktiivinenHakemus.luo_hakemus(h))
            self.jonot = []
            for jono in [hakemus.jonot for hakemus in self.hakemukset]:
                self.jonot += jono

    def close(self):
        if self.browser:
            self.browser.quit()
            self.browser = None

    def __set_asumisoikeus(self, mjono):
        search = re.search(self.__date_re, mjono)
        if search:
            self.asumisoikeus = datetime.date(
                day=int(search.group(1)),
                month=int(search.group(2)),
                year=int(search.group(3))
                )
        else:
            self.asumisoikeus = None

    def __del__(self):
        self.close()
        
